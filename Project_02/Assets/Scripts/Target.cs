﻿using UnityEngine;
using System.Collections;

public class Target : MonoBehaviour {

	public GameObject shatter;

	void Start()
	{
		transform.LookAt(Camera.main.transform);

		Destroy (gameObject, 2f);
	}

	public void OnGazeEnter()
	{
		Destroy (gameObject);
		GameObject obj = Instantiate (shatter);
		obj.transform.position = transform.position;

		ScoreManager.Instance.HitTarget ();

		Destroy (obj, 5f);
	}

	public void OnGazeExit()
	{
		
	}
}
