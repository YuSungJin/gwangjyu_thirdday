﻿using UnityEngine;
using System.Collections;

public class TargetManager : MonoBehaviour {

	public GameObject target;
	public Vector3 size = Vector3.one * 10f;
	public float cycle = 1f;

	void Start()
	{
		InvokeRepeating ("GenerateTarget", 1f, cycle);
	}

	void GenerateTarget()
	{
		GameObject obj = Instantiate (target);
		obj.transform.position = transform.position + new Vector3 (
			Random.Range(-size.x, size.x),
			Random.Range(-size.y, size.y),
			Random.Range(-size.z, size.z)
		);
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.blue;
		Gizmos.DrawWireCube (transform.position, size * 2f);
	}
}
