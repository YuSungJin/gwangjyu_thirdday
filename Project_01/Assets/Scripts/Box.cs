﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Box : EventTrigger
{

	private	Circular_Gauge	cGauge;
	private	bool			is_on;

	void Awake()
	{
		GetComponent<Renderer>().material.color = 
			new Color(Random.Range(.0f, 1.0f), Random.Range(.0f, 1.0f), Random.Range(.0f, 1.0f), 1.0f);

		cGauge	= GameObject.Find("Circular_Gauge").GetComponent<Circular_Gauge>();
		is_on	= false;
	}
	void Update()
	{
		if ( !is_on ) return;

		if ( cGauge.Is_Full )
		{
			cGauge.Exit_Gauge();
			Destroy(gameObject);
		}
	}
	public override void OnPointerEnter(PointerEventData data)
	{
		Debug.Log("OnPointerEnter called.");
		cGauge.Enter_Gauge();
		is_on = true;
	}
	public override void OnPointerExit(PointerEventData data)
	{
		Debug.Log("OnPointerExit called.");
		cGauge.Exit_Gauge();
		is_on = false;
	}
}

