﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	[SerializeField] private GameObject box_prefab;
	//[SerializeField] private Text		text_Time;
	//private	float	fTime;

	void Awake()
	{
		//fTime = .0f;
		//Update_Time();

		for ( int z = 0; z < 10; ++ z )
		{
			for ( int x = 0; x < 10; ++ x )
			{
				GameObject clone = Instantiate(box_prefab) as GameObject;
				float fx = Random.Range(-5.0f, 5.0f);
				float fy = Random.Range(-5.0f, 5.0f);
				float fz = Random.Range(-5.0f, 5.0f);
				clone.transform.position = new Vector3(fx, fy, fz);
			}
		}
	}
	void Update_Time()
	{
		//text_Time.text = string.Format("{0:D2}{1}{2:D2}", (int)fTime/60, ":", (int)fTime%60);
	}
	void Update()
	{
		//fTime += Time.deltaTime;
		//Update_Time();

		if ( Application.platform == RuntimePlatform.Android )
		{
			if ( Input.GetKeyDown(KeyCode.Escape) )
			{
	
				Application.Quit();	
			}
			else if ( Input.GetKeyDown(KeyCode.Home) )
			{

			}
			else if ( Input.GetKeyDown(KeyCode.Menu) )
			{

			}
		}
	}
}

